package controller;

import beans.Adherent;
import model.AdherentModel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "LoginServlet", urlPatterns = "/user/login")
public class LoginServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();

        String email = request.getParameter("email");
        String password = request.getParameter("password");

        AdherentModel model = new AdherentModel();
        model.setEmail(email);

        Adherent adherent = model.getAdherentByName();

        if(adherent.getPassword().equals(password)) {
            session.setAttribute("adherent" , adherent);
            request.getRequestDispatcher("/profile.jsp").forward(request,response);
        }
        else {
            response.sendRedirect("/login.html");
        }
    }
}
