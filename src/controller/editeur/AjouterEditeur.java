package controller.editeur;

import beans.Editeur;
import model.EditeurModel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "AjouterEditeur", urlPatterns="/editeur/ajouter")
public class AjouterEditeur extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String nom = request.getParameter("nom");
        String addresse = request.getParameter("addresse");

        if(nom.length() > 0 && addresse.length() > 0) {

            EditeurModel model = new EditeurModel();
            model.getEditeur().setNom(nom);
            model.getEditeur().setAddresse(addresse);

            request.getRequestDispatcher("/editeur.jsp").forward(request,response);
        }


    }
}
