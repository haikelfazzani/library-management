package controller.editeur;

import beans.Editeur;
import model.EditeurModel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "ListerServlet", urlPatterns = "/editeur")
public class ListerServlet extends HttpServlet {

    protected void doGet(HttpServletRequest req, HttpServletResponse response)
            throws ServletException, IOException {

        EditeurModel model = new EditeurModel();
        List<Editeur> editeurList = model.getListEditeur();

        if(editeurList.size() > 0 ) {
            req.setAttribute("listEditeur" , editeurList);
            req.getRequestDispatcher("/editeur.jsp").forward(req,response);
        }else {
            PrintWriter writer = response.getWriter();
            writer.print("error ");
        }
    }
}
