package controller;

import model.AdherentModel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "RegisterServlet", urlPatterns = "/user/register")
public class RegisterServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String nom = request.getParameter("nom");
        String prenom = request.getParameter("prenom");
        String addresse = request.getParameter("addresse");
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        AdherentModel model = new AdherentModel();

        model.getAdherent().setNom(nom);
        model.getAdherent().setPrenom(prenom);
        model.getAdherent().setAddresse(addresse);
        model.getAdherent().setEmail(email);
        model.getAdherent().setPassword(password);

        boolean isAdd = model.ajouter();

        if(isAdd) {
            response.sendRedirect("/login.html");
        }
        else {
            response.sendRedirect("/register.html");
        }
    }
}
