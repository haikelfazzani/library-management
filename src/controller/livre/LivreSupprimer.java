package controller.livre;

import model.LivreModel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "LivreSupprimer", urlPatterns = "/livre/supprimer")
public class LivreSupprimer extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String titre = request.getParameter("titre");
        LivreModel model = new LivreModel();
        model.setTitre(titre);
        model.supprimer();

        if(model.supprimer()) {
            request.setAttribute("livre" , model.getListLivres());
            request.getRequestDispatcher("/livre.jsp").forward(request,response);
        }
        else {
            PrintWriter writer = response.getWriter();
            writer.print("error ajout livre " + titre);
        }
    }
}
