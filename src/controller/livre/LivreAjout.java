package controller.livre;

import beans.Editeur;
import model.EditeurModel;
import model.LivreModel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "LivreAjout", urlPatterns = "/livre/ajout")
public class LivreAjout extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String titre = request.getParameter("titre");
        Double prix = Double.parseDouble(request.getParameter("prix"));
        String dateAchat = request.getParameter("dateAchat");
        String editeurName = request.getParameter("editeur");

        LivreModel model = new LivreModel();
        model.getLivre().setTitre(titre);
        model.getLivre().setPrix(prix);
        model.getLivre().setDateAchat(dateAchat);

        EditeurModel editeurModel = new EditeurModel();
        editeurModel.setNom(editeurName);
        Editeur editeur = editeurModel.getEditeurByName();

        model.getLivre().setEditeur(editeur);

        if(model.ajouter()) {
            request.setAttribute("livre" , model.getListLivres());
            request.getRequestDispatcher("/livre.jsp").forward(request,response);
        }
        else {
            PrintWriter writer = response.getWriter();
            writer.print("error ajout livre " + editeur.getNom());
        }

    }
}
