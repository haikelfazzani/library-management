package controller.livre;

import model.LivreModel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "LivreLister", urlPatterns = "/livre")
public class LivreLister extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        LivreModel model = new LivreModel();
        request.setAttribute("livre" , model.getListLivres());
        request.getRequestDispatcher("/livre.jsp").forward(request,response);
    }
}
