package controller.emprunt;

import model.EmpruntModel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "EmpruntLister", urlPatterns = "/emprunt")
public class EmpruntLister extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        EmpruntModel model = new EmpruntModel();

        request.setAttribute("emprunts" , model.lister());
        request.getRequestDispatcher("/emprunt.jsp").forward(request,response);

    }
}
