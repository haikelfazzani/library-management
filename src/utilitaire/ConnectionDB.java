package utilitaire;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionDB {

    private static Connection conn = null;

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/gestionbib" , "root" , "");
            System.out.println("connected ");
        }
        catch (ClassNotFoundException | SQLException exc) {
            throw new RuntimeException("not connected :: ! ! " , exc);
        }
    }

    public static Connection getConnection() {
        return conn;
    }
}
