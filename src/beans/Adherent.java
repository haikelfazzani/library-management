package beans;

public class Adherent {

    private int idAdherent;
    private String nom , prenom, addresse, email, password;

    public Adherent() {}

    public Adherent(int idAdherent, String nom, String prenom, String addresse, String email, String password) {
        this.idAdherent = idAdherent;
        this.nom = nom;
        this.prenom = prenom;
        this.addresse = addresse;
        this.email = email;
        this.password = password;
    }

    public int getIdAdherent() {
        return idAdherent;
    }

    public void setIdAdherent(int idAdherent) {
        this.idAdherent = idAdherent;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAddresse() {
        return addresse;
    }

    public void setAddresse(String addresse) {
        this.addresse = addresse;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
