package beans;

import java.util.List;

public class Editeur {

    private int idEditeur;
    private String nom , addresse;
    private List<Livre> livreList;

    public Editeur() { }

    public Editeur(String nom, String addresse) {
        this.nom = nom;
        this.addresse = addresse;
    }

    public Editeur(String nom, String addresse, List<Livre> livreList) {
        this.nom = nom;
        this.addresse = addresse;
        this.livreList = livreList;
    }

    public void setIdEditeur(int idEditeur) {
        this.idEditeur = idEditeur;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Livre> getLivreList() {
        return livreList;
    }

    public void setLivreList(List<Livre> livreList) {
        this.livreList = livreList;
    }

    public int getIdEditeur() {
        return idEditeur;
    }

    public String getAddresse() {
        return addresse;
    }

    public void setAddresse(String addresse) {
        this.addresse = addresse;
    }
}
