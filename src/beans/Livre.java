package beans;

public class Livre {

    private int codeLivre;
    private String titre;
    private double prix;
    private String dateAchat;
    private Editeur editeur;

    public Livre() {}

    public Livre(String titre, double prix, String dateAchat) {
        this.titre = titre;
        this.prix = prix;
        this.dateAchat = dateAchat;
    }

    public Livre(String titre, double prix, String dateAchat, Editeur editeur) {
        this.titre = titre;
        this.prix = prix;
        this.dateAchat = dateAchat;
        this.editeur = editeur;
    }

    public Livre(int codeLivre, String titre, double prix, String dateAchat) {
        this.codeLivre = codeLivre;
        this.titre = titre;
        this.prix = prix;
        this.dateAchat = dateAchat;
    }

    public void setCodeLivre(int codeLivre) {
        this.codeLivre = codeLivre;
    }

    public int getCodeLivre() {
        return codeLivre;

    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public String getDateAchat() {
        return dateAchat;
    }

    public void setDateAchat(String dateAchat) {
        this.dateAchat = dateAchat;
    }

    public Editeur getEditeur() {
        return editeur;
    }

    public void setEditeur(Editeur editeur) {
        this.editeur = editeur;
    }

}
