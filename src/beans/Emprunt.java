package beans;

public class Emprunt {

    private Livre livre;
    private Adherent adherent;
    private String dateEmprunt;

    public Emprunt() {
    }

    public Emprunt(Livre livre, Adherent adherent, String dateEmprunt) {
        this.livre = livre;
        this.adherent = adherent;
        this.dateEmprunt = dateEmprunt;
    }

    public Livre getLivre() {
        return livre;
    }

    public void setLivre(Livre livre) {
        this.livre = livre;
    }

    public Adherent getAdherent() {
        return adherent;
    }

    public void setAdherent(Adherent adherent) {
        this.adherent = adherent;
    }

    public String getDateEmprunt() {
        return dateEmprunt;
    }

    public void setDateEmprunt(String dateEmprunt) {
        this.dateEmprunt = dateEmprunt;
    }
}
