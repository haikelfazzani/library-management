package dao;

import beans.Editeur;
import beans.Livre;
import utilitaire.ConnectionDB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class LivreDao implements IDao<Livre> {

    private static Connection conn;
    private PreparedStatement stmt;
    private ResultSet resultSet;

    public LivreDao() { conn = ConnectionDB.getConnection(); }

    @Override
    public boolean ajouter(Livre o) {
        String sql = "INSERT INTO LIVRE(titre, prix, date_achat, id_editeur) VALUES(?, ?, ? , ?)";
        try {
            stmt = conn.prepareStatement(sql);

            stmt.setString(1, o.getTitre());
            stmt.setDouble(2, o.getPrix());
            stmt.setString(3, o.getDateAchat());
            stmt.setInt(4, o.getEditeur().getIdEditeur());

            stmt.executeUpdate();
            return true;
        }
        catch (Exception ex) {
            return false;
        }
    }

    @Override
    public boolean modifier(String titre, Livre o) {
        return false;
    }

    @Override
    public boolean supprimer(String titre) {
        String sql = "DELETE FROM LIVRE WHERE titre = ?";
        try {

            PreparedStatement stamt = conn.prepareStatement(sql);
            stamt.setString(1, titre);
            stamt.executeUpdate();

            return true;
        }
        catch (Exception ex) {
            return false;
        }
    }

    @Override
    public Livre getObject(String titre) {
        String sql = "SELECT * FROM LIVRE WHERE titre = ? LIMIT 1";
        Livre livre = new Livre();
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, titre);
            resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                livre.setCodeLivre(resultSet.getInt(1));
                livre.setTitre(resultSet.getString(2));
                livre.setPrix(resultSet.getDouble(3));
                livre.setDateAchat(resultSet.getString(4));
                livre.setEditeur(getEditeurByLivre(resultSet.getInt(5)));
            }

        }
        catch (Exception ex) {
            throw new RuntimeException("no livre found : ", ex);
        }
        return livre;
    }

    @Override
    public List<Livre> lister() {
        String sql = "SELECT * FROM LIVRE";
        List<Livre> livreList = new ArrayList<>();
        try {
            stmt = conn.prepareStatement(sql);
            resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                Livre livre = new Livre();

                livre.setCodeLivre(resultSet.getInt(1));
                livre.setTitre(resultSet.getString(2));
                livre.setPrix(resultSet.getDouble(3));
                livre.setDateAchat(resultSet.getString(4));
                livre.setEditeur(getEditeurByLivre(resultSet.getInt(5)));

                livreList.add(livre);
            }

        }
        catch (Exception ex) {
            throw new RuntimeException("no list livre found : ", ex);
        }
        return livreList;
    }

    public Editeur getEditeurByLivre(int idEditeur) {
        Editeur editeur = new Editeur();
        String sql = "SELECT * FROM EDITEUR WHERE id_editeur = ?";
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, idEditeur);
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                editeur.setIdEditeur(result.getInt(1));
                editeur.setNom(result.getString(2));
                editeur.setAddresse(result.getString(3));
            }
        }
        catch (Exception excc) {
            throw new RuntimeException("no editeur found by livre : ", excc);
        }
        return editeur;
    }
}
