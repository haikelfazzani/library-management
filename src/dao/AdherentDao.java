package dao;

import beans.Adherent;
import utilitaire.ConnectionDB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class AdherentDao implements IDao<Adherent> {

    private static Connection conn = null;
    private PreparedStatement stmt = null;
    private ResultSet resultSet = null;

    public AdherentDao() { conn = ConnectionDB.getConnection(); }

    @Override
    public boolean ajouter(Adherent o) {
        String sql = "INSERT INTO adherent(nom, prenom ,addresse, email, password) VALUES(?, ?, ?, ?, ?)";
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setString(1 , o.getNom());
            stmt.setString(2 , o.getPrenom());
            stmt.setString(3 , o.getAddresse());
            stmt.setString(4 , o.getEmail());
            stmt.setString(5 , o.getPassword());
            stmt.executeUpdate();

            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean modifier(String email, Adherent o) {
        String sql = "UPDATE ADHERENT SET nom = ?, prenom = ?, ADDRESSE = ?, email = ?, password = ? WHERE email = ?";
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setString(1 , o.getNom());
            stmt.setString(2 , o.getPrenom());
            stmt.setString(3 , o.getAddresse());
            stmt.setString(4 , o.getEmail());
            stmt.setString(5 , o.getPassword());
            stmt.setString(6 , email);
            stmt.executeUpdate();
            stmt.close();
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean supprimer(String email) {
        String sql = "DELETE FROM ADHERENT WHERE email = ?";
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setString(1 , email);
            stmt.executeUpdate();

            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    @Override
    public Adherent getObject(String email) {
        Adherent adherent = null;
        String sql = "SELECT * FROM adherent WHERE email = ? LIMIT 1";
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setString(1 , email);
            resultSet = stmt.executeQuery();

            while(resultSet.next()) {
                adherent = new Adherent(
                        resultSet.getInt(1),
                        resultSet.getString(2) ,
                        resultSet.getString(3) ,
                        resultSet.getString(4),
                        resultSet.getString(5),
                        resultSet.getString(6)
                );
            }
            stmt.close();
        }
        catch (Exception e) {
            throw new RuntimeException("no adherent found : " , e);
        }
        return adherent;
    }

    @Override
    public List<Adherent> lister() {
        List<Adherent> adherentList = new ArrayList<>();
        String sql = "SELECT * FROM adherent";
        try {
            stmt = conn.prepareStatement(sql);
            resultSet = stmt.executeQuery();

            while(resultSet.next()) {
                Adherent adherent = new Adherent(
                        resultSet.getInt(1),
                        resultSet.getString(2) ,
                        resultSet.getString(3) ,
                        resultSet.getString(4),
                        resultSet.getString(5),
                        resultSet.getString(6)
                );
                adherentList.add(adherent);
            }
            stmt.close();
        }
        catch (Exception e) {
            throw new RuntimeException("no list adherent found : " , e);
        }
        return adherentList;
    }
}
