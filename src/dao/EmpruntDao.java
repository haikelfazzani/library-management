package dao;

import beans.Adherent;
import beans.Emprunt;
import beans.Livre;
import utilitaire.ConnectionDB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class EmpruntDao {

    private static Connection conn = null;
    private PreparedStatement stmt = null;
    private ResultSet resultSet = null;

    public EmpruntDao() { conn = ConnectionDB.getConnection(); }

    public boolean ajouter(Emprunt o) {
        String sql = "INSERT INTO EMPRUNT(code_livre, id_adherent, date_emprunt) VALUES(?, ?, ?)";
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1 , o.getLivre().getCodeLivre());
            stmt.setInt(2 , o.getAdherent().getIdAdherent());
            stmt.setString(3, o.getDateEmprunt());
            stmt.executeUpdate();
            stmt.close();
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    public Emprunt getObject(String dateEmprunt) {
        Emprunt emprunt = new Emprunt();
        String sql = "SELECT * from emprunt WHERE date_emprunt = ? LIMIT 1";
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setString(1 , dateEmprunt);

            resultSet = stmt.executeQuery();

            while(resultSet.next()) {
                emprunt.setLivre(getLivre(resultSet.getInt(1)));
                emprunt.setAdherent(getAdherent(resultSet.getInt(2))); ;
                emprunt.setDateEmprunt(resultSet.getString(3));
            }
        }
        catch (Exception e) {
            throw new RuntimeException("no editeur found : " , e);
        }
        return emprunt;
    }

    public List<Emprunt> lister() {
        List<Emprunt> emprunts = new ArrayList<>();
        String sql = "SELECT * from emprunt";
        try {
            stmt = conn.prepareStatement(sql);
            ResultSet result = stmt.executeQuery();

            while(result.next()) {
                Emprunt emprunt = new Emprunt();
                emprunt.setLivre(getLivre(result.getInt(1)));
                emprunt.setAdherent(getAdherent(result.getInt(2))); ;
                emprunt.setDateEmprunt(result.getString(3));

                emprunts.add(emprunt);
            }
        }
        catch (Exception e) {
            throw new RuntimeException("no emprunt list found : " , e);
        }
        return emprunts;
    }

    private Livre getLivre(int codeLivre) {
        String sql = "SELECT * FROM LIVRE WHERE code_livre = ? LIMIT 1";
        Livre livre = new Livre();
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, codeLivre);
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                livre.setCodeLivre(result.getInt(1));
                livre.setTitre(result.getString(2));
                livre.setPrix(result.getDouble(3));
                livre.setDateAchat(result.getString(4));
            }

        }
        catch (Exception ex) {
            throw new RuntimeException("Emprunt : no livre found : ", ex);
        }
        return livre;
    }

    private Adherent getAdherent(int idAdherent) {
        Adherent adherent = null;
        String sql = "SELECT * FROM adherent WHERE id_adherent = ? LIMIT 1";
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1 , idAdherent);
            ResultSet result = stmt.executeQuery();

            while(result.next()) {
                adherent = new Adherent(
                        result.getInt(1),
                        result.getString(2) ,
                        result.getString(3) ,
                        result.getString(4),
                        result.getString(5),
                        result.getString(6)
                );
            }
            stmt.close();
        }
        catch (Exception e) {
            throw new RuntimeException("no adherent found : " , e);
        }
        return adherent;
    }
}
