package dao;

import beans.Editeur;
import beans.Livre;
import utilitaire.ConnectionDB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class EditeurDao implements IDao<Editeur> {

    private static Connection conn = null;
    private PreparedStatement stmt = null;
    private ResultSet resultSet = null;

    public EditeurDao(){ conn = ConnectionDB.getConnection(); }

    @Override
    public boolean ajouter(Editeur o) {
        String sql = "INSERT INTO EDITEUR(nom,addresse) VALUES(?,?)";
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setString(1 , o.getNom());
            stmt.setString(2 , o.getAddresse());
            stmt.executeUpdate();
            stmt.close();
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean modifier(String nom, Editeur o) {
        String sql = "UPDATE EDITEUR SET nom = ? , ADDRESSE = ? WHERE nom = ?";
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setString(1 , o.getNom());
            stmt.setString(2 , o.getAddresse());
            stmt.setString(3 , nom);
            stmt.executeUpdate();
            stmt.close();
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    public boolean supprimer(String nom) {
        String sql = "DELETE FROM EDITEUR WHERE nom = ?";
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setString(1 , nom);
            stmt.executeUpdate();
            stmt.close();
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    @Override
    public Editeur getObject(String nom) {
        Editeur editeur = new Editeur();
        String sql = "SELECT * from EDITEUR WHERE nom = ? LIMIT 1";
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setString(1 , nom);

            resultSet = stmt.executeQuery();

            while(resultSet.next()) {
                editeur.setIdEditeur(resultSet.getInt(1));
                editeur.setNom(resultSet.getString(2)) ;
                editeur.setAddresse(resultSet.getString(3));
            }
        }
        catch (Exception e) {
            throw new RuntimeException("no editeur found : " , e);
        }
        return editeur;
    }

    @Override
    public List<Editeur> lister() {
        String sql = "SELECT * FROM editeur";
        List<Editeur> editeurList = new ArrayList<>();
        try {
            stmt = conn.prepareStatement(sql);
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                Editeur editeur = new Editeur();
                int id = result.getInt(1);
                editeur.setIdEditeur(id);
                editeur.setLivreList(getLivresByEditeur(id));
                editeur.setNom(result.getString(2));
                editeur.setAddresse(result.getString(3));

                editeurList.add(editeur);
            }
            stmt.close();
        }
        catch (Exception e) {
            throw new RuntimeException("no editeur list found : " , e);
        }
        return editeurList;
    }

    public List<Livre> getLivresByEditeur(int id) {
        List<Livre> livreList = new ArrayList<>();
        String sql = "SELECT * FROM LIVRE WHERE id_editeur = ?";
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1,id);
            resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                livreList.add(
                        new Livre(
                                resultSet.getInt(1),
                                resultSet.getString(2),
                                resultSet.getDouble(3),
                                resultSet.getString(4)
                        )
                );
            }
        }
        catch (Exception e) {
            throw new RuntimeException("no livre list found : " , e);
        }
        return livreList;
    }
}
