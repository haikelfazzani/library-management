package model;

import beans.Editeur;
import dao.EditeurDao;

import java.util.List;

public class EditeurModel {

    private Editeur editeur = null;
    private EditeurDao dao = null;
    private String nom = "";

    public EditeurModel() { editeur = new Editeur(); dao = new EditeurDao(); }

    // Crud methods
    public boolean ajouter() { return dao.ajouter(editeur); }
    public boolean modifier() { return dao.modifier(nom,editeur); }
    public boolean supprimer() { return dao.supprimer(nom); }
    public Editeur getEditeurByName() { return dao.getObject(nom); }
    public List<Editeur> getListEditeur() { return dao.lister(); }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Editeur getEditeur() {
        return editeur;
    }

    public void setEditeur(Editeur editeur) {
        this.editeur = editeur;
    }
}
