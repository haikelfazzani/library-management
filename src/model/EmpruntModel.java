package model;

import beans.Emprunt;
import dao.EmpruntDao;

import java.util.List;

public class EmpruntModel {

    private Emprunt emprunt = null;
    private EmpruntDao dao = null;
    private String dateEmprunt = "";

    public EmpruntModel() {
        emprunt = new Emprunt();
        dao = new EmpruntDao();
    }

    public boolean ajouter() { return dao.ajouter(emprunt); }
    public Emprunt getEmpruntByDate() { return dao.getObject(dateEmprunt); }
    public List<Emprunt> lister() { return dao.lister(); }

    public Emprunt getEmprunt() {
        return emprunt;
    }

    public void setEmprunt(Emprunt emprunt) {
        this.emprunt = emprunt;
    }

    public String getDateEmprunt() {
        return dateEmprunt;
    }

    public void setDateEmprunt(String dateEmprunt) {
        this.dateEmprunt = dateEmprunt;
    }
}
