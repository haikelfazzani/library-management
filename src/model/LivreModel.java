package model;

import beans.Livre;
import dao.LivreDao;

import java.util.List;

public class LivreModel {

    private Livre livre = null;
    private LivreDao dao = null;
    private String titre = "";

    public LivreModel() {
        livre = new Livre();
        dao = new LivreDao();
    }

    // CRUD
    public boolean ajouter() { return dao.ajouter(livre); }
    public boolean modifier() { return dao.modifier(titre, livre); }
    public boolean supprimer() { return dao.supprimer(titre); }
    public Livre getLivreByTitre() { return dao.getObject(titre); }
    public List<Livre> getListLivres() { return dao.lister(); }


    // GETTER AND SETTER
    public Livre getLivre() {
        return livre;
    }

    public void setLivre(Livre livre) {
        this.livre = livre;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }
}
