package model;

import beans.Adherent;
import dao.AdherentDao;

import java.util.List;

public class AdherentModel {

    private Adherent adherent;
    private AdherentDao dao;
    private String email = "";

    public AdherentModel() {
        adherent = new Adherent();
        dao = new AdherentDao();
    }

    public boolean ajouter() { return dao.ajouter(adherent); }
    public boolean modifier() { return dao.modifier(email,adherent);}
    public boolean supprimer() { return dao.supprimer(email); }
    public Adherent getAdherentByName() { return dao.getObject(email); }
    public List<Adherent> lister() { return dao.lister(); }

    public Adherent getAdherent() { return adherent; }

    public void setAdherent(Adherent adherent) { this.adherent = adherent; }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
