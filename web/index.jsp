<%@ page import="beans.Livre" %>
<%@ page import="java.util.List" %>
<%@include file="/partials/head.jsp"%>
  <body>
  
    <%@include file="partials/nav.jsp"%>

    <div class="container mt-5 mb-5">
        <div class="row">
            <div class="col-12 mt-3">
                <h2><i class="fas fa-search"></i> CHERCHER UN LIVRE</h2>
            </div>
            <div class="col-md-12">
                <form>
                    <div class="form-row">
                        <div class="col-10">
                            <input type="text" class="form-control" placeholder="Chercher par titre ..">
                        </div>

                        <div class="col">
                            <button type="submit" class="btn btn-success btn-block">CHERCHER</button>
                        </div>
                    </div>
                </form>
            </div>


        <div class="col-12 mt-3">
            <h2><i class="fas fa-list"></i> LISTE DES LIVRES</h2>
        </div>

            <%
                if(request.getAttribute("livre") != null) {
                    List<Livre> livreList = (List<Livre>)request.getAttribute("livre");
                    if(livreList.size() > 0) {
                        int i = 0;
                        for(Livre l : livreList) {%>

            <div class="col-md-3 mb-5">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title"><i class="fas fa-book"></i> <%= l.getTitre()%></h5>
                        <h6 class="card-subtitle mb-2 text-muted">
                            <i class="fas fa-hand-holding-usd"></i> <%= l.getPrix() %>
                        </h6>
                        <h6 class="card-subtitle mb-2 text-muted">
                            <i class="fas fa-clock"></i> <%= l.getDateAchat() %>
                        </h6>
                        <h6 class="card-subtitle mb-2 text-muted">
                            <i class="fas fa-pen-square"></i> <%= l.getEditeur().getNom() %>
                        </h6>
                        <h6 class="card-subtitle mb-2 text-muted">
                            <i class="fas fa-map-marker-alt"></i> <%= l.getEditeur().getAddresse() %>
                        </h6>
                        <a href="#" class="btn btn-success" style="float:right;">
                            <i class="fas fa-plus-square"></i>
                        </a>
                    </div>
                </div>
            </div>
            <%} } }%>

        </div><!-- /ROW -->
    </div>
  </body>
</html>
