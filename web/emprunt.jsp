<%@ page import="java.util.List" %>
<%@ page import="beans.Editeur" %>
<%@ page import="beans.Emprunt" %>
<%@include file="/partials/head.jsp"%>
<body>

<%@include file="partials/nav.jsp"%>

<div class="container mt-5">
    <h2><i class="fas fa-plus"></i> Ajouter Un Editeur</h2>

    <form action="/emprunt/ajout" method="post">
        <div class="form-row mb-3">
            <div class="col-6">
                <input type="text" name="titre" class="form-control" placeholder="titre">
            </div>
            <div class="col-5">
                <input type="text" name="nom" class="form-control" placeholder="nom">
            </div>
            <div class="col-1">
                <button type="submit" class="btn btn-success btn-block">
                    <i class="fas fa-plus"></i>
                </button>
            </div>
        </div>
    </form>

    <% if(request.getAttribute("emprunts") != null) {
        List<Emprunt> empruntList = (List<Emprunt>) request.getAttribute("emprunts");
        int i = 0;
    %>

    <h2><i class="fas fa-list mt-5 mb-0"></i> Liste Des Editeurs</h2>
    <table class="table mb-5">
        <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Lire Titre</th>
            <th scope="col">Nom Adherent</th>
            <th scope="col">Date emprunt</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        <% for(Emprunt e : empruntList) {%>

        <tr>
            <td><%= ++i %></td>
            <td><%= e.getLivre().getTitre() %></td>
            <td><%= e.getAdherent().getNom() %></td>
            <td><%= e.getDateEmprunt() %></td>
            <td>
                <a class="btn btn-success" href="#"><i class="fas fa-pen-square"></i></a>
                <a class="btn btn-danger" href="#"><i class="fas fa-trash"></i></a>
            </td>
        </tr>
        <% }%>
        </tbody>
    </table>
    <% }%>
</div>

</body>
</html>
