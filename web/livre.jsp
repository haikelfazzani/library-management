<%@ page import="model.EditeurModel" %>
<%@ page import="java.util.List" %>
<%@ page import="beans.Editeur" %>
<%@ page import="beans.Livre" %>
<%@include file="/partials/head.jsp"%>
<body>

<%@include file="partials/nav.jsp"%>

<div class="container mt-5">
    <h2><i class="fas fa-plus"></i> Ajouter Un Livre</h2>

    <form action="/livre/ajout" method="post">
        <div class="form-row mb-3">
            <div class="col-3">
                <input type="text" name="titre" class="form-control" placeholder="Titre">
            </div>
            <div class="col-3">
                <input type="text" name="prix" class="form-control" placeholder="Prix">
            </div>
            <div class="col-2">
                <input type="text" name="dateAchat" class="form-control" placeholder="Date achat">
            </div>

            <div class="col-3">
                <select class="form-control" name="editeur">
                    <option value="Choisir un Editeur" selected>Choisir un Editeur</option>
            <%
                EditeurModel model = new EditeurModel();
                List<Editeur> editeurList = model.getListEditeur();
                if(editeurList.size() > 0) {
                    for(Editeur e : editeurList) {
            %>
                    <option value="<%= e.getNom() %>"><%= e.getNom() %></option>
                <%}}%>
                </select>
            </div>

            <div class="col">
                <button type="submit" class="btn btn-success btn-block">
                    <i class="fas fa-plus"></i>
                </button>
            </div>

        </div>
    </form>

    <h2><i class="fas fa-list mt-5 mb-0"></i> Liste Des Livres</h2>
    <table class="table mb-5">
        <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Titre</th>
            <th scope="col">Prix</th>
            <th scope="col">Date Achat</th>
            <th scope="col">Editeur</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        <%
            if(request.getAttribute("livre") != null) {
                List<Livre> livreList = (List<Livre>)request.getAttribute("livre");
                if(livreList.size() > 0) {
                    int i = 0;
                    for(Livre l : livreList) {%>
                        <tr>
                            <td><%= ++i %></td>
                            <td><%= l.getTitre() %></td>
                            <td><%= l.getPrix() %></td>
                            <td><%= l.getDateAchat() %></td>
                            <td><%= l.getEditeur().getNom() %></td>
                            <td>
                                <a class="btn btn-success" href="/livre/modifier?titre=<%= l.getTitre() %>">
                                    <i class="fas fa-pen-square"></i>
                                </a>
                                <a class="btn btn-danger" href="/livre/supprimer?titre=<%= l.getTitre() %>">
                                    <i class="fas fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    <%} } }%>

        </tbody>
    </table>

</div>

</body>
</html>
