<%@ page import="beans.Adherent" %>
<%@include file="/partials/head.jsp"%>
<body>

<%@include file="partials/nav.jsp"%>

<div class="container mt-5">
    <div class="row">
        <% Adherent adherent = (Adherent) session.getAttribute("adherent");
            if(adherent != null) {%>
        <div class="col-12">
            <h2><i class="fas fa-user"></i> <%= adherent.getNom() %></h2>
        </div>
        <div class="col-4 img-profile">
            <img src="/assets/img/1.png" alt="profile">
        </div>

        <div class="col-8 mb-5">

            <form class="form-profile">
                <div class="form-group">
                    <label>Nom</label>
                    <input type="text" value="<%= adherent.getNom() %>" class="form-control" placeholder="Enter nom">
                </div>

                <div class="form-group">
                    <label>Prenom</label>
                    <input type="text" value="<%= adherent.getPrenom() %>" class="form-control" placeholder="Enter prenom">
                </div>

                <div class="form-group">
                    <label>Addresse</label>
                    <input type="text" value="<%= adherent.getAddresse() %>" class="form-control" placeholder="Enter addresse">
                </div>

                <div class="form-group">
                    <label>Email address</label>
                    <input type="email" value="<%= adherent.getEmail() %>" class="form-control" placeholder="Enter email">
                </div>

                <div class="form-group">
                    <label>Password</label>
                    <input type="password" value="<%= adherent.getPassword() %>" class="form-control" placeholder="Password">
                </div>

                <button type="submit" class="btn btn-success">METTRE A JOUR</button>
            </form>
            <a class="btn btn-danger" style="float: right">SUPPRIMER</a>
            <% }%>
        </div>

    </div><!-- /Row -->
</div>

</body>
</html>
