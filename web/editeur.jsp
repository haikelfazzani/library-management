<%@ page import="java.util.List" %>
<%@ page import="beans.Editeur" %>
<%@ page import="beans.Livre" %>
<%@include file="/partials/head.jsp"%>
<body>

<%@include file="partials/nav.jsp"%>

    <div class="container mt-5">
        <h2><i class="fas fa-plus"></i> Ajouter Un Editeur</h2>

        <form action="/editeur/ajouter" method="post">
            <div class="form-row mb-3">
                <div class="col-6">
                    <input type="text" name="nom" class="form-control" placeholder="Nom">
                </div>
                <div class="col-5">
                    <input type="text" name="addresse" class="form-control" placeholder="Addresse">
                </div>
                <div class="col-1">
                    <button type="submit" class="btn btn-success btn-block">
                        <i class="fas fa-plus"></i>
                    </button>
                </div>
            </div>
        </form>

        <% if(request.getAttribute("listEditeur") != null) {
            List<Editeur> editeurList = (List<Editeur>) request.getAttribute("listEditeur");
            int i = 0;
        %>

        <h2><i class="fas fa-list mt-5 mb-0"></i> Liste Des Editeurs</h2>
        <table class="table mb-5">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nom</th>
                <th scope="col">Addresse</th>
                <th scope="col">Livre</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            <% for(Editeur e : editeurList) {%>

            <tr>
                <td><%= ++i %></td>
                <td><%= e.getNom() %></td>
                <td><%= e.getAddresse() %></td>
                <td>
                    <select class="form-control" name="editeur">
                        <% List<Livre> livreList = e.getLivreList();
                            for(Livre l : livreList) {
                                if(l.getTitre().length() > 0) {

                        %>
                                <option value="<%= l.getTitre() %>"><%= l.getTitre()%></option>
                            <%} else {%>
                                    <option value="aucun livre">aucun livre</option>
                                <%}
                            }%>
                    </select>
                </td>
                <td>
                    <a class="btn btn-success" href="#"><i class="fas fa-pen-square"></i></a>
                    <a class="btn btn-danger" href="#"><i class="fas fa-trash"></i></a>
                </td>
            </tr>
            <% }%>
            </tbody>
        </table>
        <% }%>
    </div>

</body>
</html>
