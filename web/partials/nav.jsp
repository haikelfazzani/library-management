<%@page import="beans.Adherent" %>
<nav class="navbar navbar-expand-md navbar-light bg-light">
    <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/acceuil" style="color: #dc3545;">
                    <i class="fas fa-book"></i> Bibliotheque
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/acceuil">Acceuil</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/editeur">Editeur</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/livre">Livre</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/emprunt">Emprunt</a>
            </li>
        </ul>
    </div>

    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
        <ul class="navbar-nav ml-auto">

            <%
                Adherent ad = (Adherent) session.getAttribute("adherent");
                if(ad != null) {
            %>

            <li class="nav-item">
                <a class="btn btn-danger" href="${pageContext.servletContext.contextPath}/profile.jsp"><%= ad.getNom() %></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="${pageContext.servletContext.contextPath}/user/logout">Logout</a>
            </li>

            <%} else { %>
            <li class="nav-item">
                <a class="nav-link" href="${pageContext.servletContext.contextPath}/login.html">Login</a>
            </li>

            <li class="nav-item">
                <a class="btn btn-danger" href="${pageContext.servletContext.contextPath}/register.html">Register</a>
            </li>
            <% } %>
        </ul>
    </div>
</nav>